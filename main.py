from fastapi import FastAPI, HTTPException
from fastapi.responses import JSONResponse
from pydantic import BaseModel
from pymongo import MongoClient
from bson import ObjectId
from typing import List


# Initialisation de l'application FastAPI
app = FastAPI()

# Connexion à la base de données MongoDB
try:
    client = MongoClient("mongodb://mongo:27017/")
    db = client["Carparts"]
    collection = db["stocks"]
    print("Connexion à la base de données réussie.")
except Exception as e:
    print(f"Erreur lors de la connexion à la base de données : {str(e)}")


# Modèle Pydantic pour la création d'un produit
class ProductCreate(BaseModel):
    name: str
    description: str
    quantity: int

# Modèle Pydantic pour la modification d'un produit
class ProductUpdate(BaseModel):
    description: str
    quantity: int

# Modèle Pydantic pour la réponse JSON
class ProductResponse(BaseModel):
    name: str
    description: str
    quantity: int

# Endpoint pour ajouter un produit
@app.post("/products/", response_model=ProductResponse)
async def create_product(product: ProductCreate):
    product_data = dict(product)
    result = collection.insert_one(product_data)
    product_data["_id"] = str(result.inserted_id)
    return JSONResponse(content=product_data)

# Endpoint pour obtenir le descriptif et la quantité d'un produit
@app.get("/products/{product_id}", response_model=ProductResponse)
async def read_product(product_id: str):
    product = collection.find_one({"_id": ObjectId(product_id)})
    if product:
        serialized_product = {
            "_id": str(product["_id"]),  # Convert ObjectId to string
            "name": product["name"],
            "description": product["description"],
            "quantity": product["quantity"],
        }
        return JSONResponse(content=serialized_product)
    raise HTTPException(status_code=404, detail="Product not found")


# Endpoint pour modifier un produit
@app.put("/products/{product_id}", response_model=ProductResponse)
async def update_product(product_id: str, update: ProductUpdate):
    updated_product = collection.find_one_and_update(
        {"_id": ObjectId(product_id)},
        {"$set": dict(update)},
        return_document=True
    )
    if updated_product:
        serialized_product = {
            "_id": str(updated_product["_id"]),  # Convert ObjectId to string
            "name": updated_product["name"],
            "description": updated_product["description"],
            "quantity": updated_product["quantity"],
        }
        return JSONResponse(content=serialized_product)
    raise HTTPException(status_code=404, detail="Product not found")

# Endpoint pour supprimer un produit
@app.delete("/products/{product_id}", response_model=dict)
async def delete_product(product_id: str):
    result = collection.delete_one({"_id": ObjectId(product_id)})
    if result.deleted_count:
        return {"message": "Product deleted successfully"}
    raise HTTPException(status_code=404, detail="Product not found")

# Endpoint pour obtenir tous les produits
@app.get("/products/", response_model=List[ProductResponse])
async def get_all_products():
    products = collection.find()
    
    serialized_products = []
    for product in products:
        serialized_product = {
            "_id": str(product["_id"]),  # Convert ObjectId to string
            "name": product["name"],
            "description": product["description"],
            "quantity": product["quantity"],
        }
        serialized_products.append(serialized_product)

    return JSONResponse(content=serialized_products)
